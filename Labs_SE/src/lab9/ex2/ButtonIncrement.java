package lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonIncrement extends JFrame {

    JTextField noDisplay;
    JButton bIncrement;
    int counter = 0;

    ButtonIncrement(){
        setTitle("Button Increment");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        noDisplay = new JTextField();
        noDisplay.setBounds(100,100,300,50);
        noDisplay.setText(String.valueOf(counter));

        bIncrement = new JButton("Increment!");
        bIncrement.setBounds(100,200,300,50);

        bIncrement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                noDisplay.setText(String.valueOf(counter));
            }
        });

        add(noDisplay);
        add(bIncrement);
    }

    public static void main(String[] args) {
        new ButtonIncrement();
    }
}
