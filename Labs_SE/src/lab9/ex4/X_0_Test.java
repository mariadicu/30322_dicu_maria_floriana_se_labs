package lab9.ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class X_0_Test {
    public static void main(String[] args) {
        new TicTacToeFrame();
    }
}

class TicTacToeFrame extends JFrame{
    public JButton [][] jButtons = new JButton[3][3];
    public JButton reset;
    public JTextField winner;
    public int turn;
    public Game game = new Game();

    public TicTacToeFrame(){
        setTitle("TIC TAC TOE");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();

        setSize(1000, 1000);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        Font font1 = new Font("SansSerif",Font.BOLD, 30);
        Font font2 = new Font("SansSerif",Font.BOLD, 42);

        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                jButtons[i][j] = new JButton();
                jButtons[i][j].setText("---");
            }
        }
        jButtons[0][0].setBounds(250,250,100,100);
        jButtons[0][1].setBounds(450,250,100,100);
        jButtons[0][2].setBounds(650,250,100,100);
        jButtons[1][0].setBounds(250,450,100,100);
        jButtons[1][1].setBounds(450,450,100,100);
        jButtons[1][2].setBounds(650,450,100,100);
        jButtons[2][0].setBounds(250,650,100,100);
        jButtons[2][1].setBounds(450,650,100,100);
        jButtons[2][2].setBounds(650,650,100,100);



        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                jButtons[i][j].setFont(font1);
                jButtons[i][j].addActionListener(new ButtonAction());
                add(jButtons[i][j]);
            }
        }

        winner = new JTextField();
        winner.setBounds(300, 50, 400,150);
        winner.setFont(font2);
        winner.setHorizontalAlignment(JTextField.CENTER);
        add(winner);

        reset = new JButton("RESET");
        reset.setBounds(450,800,100,100);
        reset.addActionListener(new ResetButton());
        add(reset);
    }

    class ButtonAction implements ActionListener{

        public void actionPerformed(ActionEvent e){
            JButton b=(JButton)e.getSource();
            if(turn % 2 == 0){
                b.setText("X");
            }
            else{
                b.setText("0");
            }
            turn++;
            if(game.isWinner()){
                winner.setText("The winner is: " + game.whoIsWinner());
            }
        }
    }

    class ResetButton implements ActionListener{
        public void actionPerformed(ActionEvent e){
            turn = 0;
            for(int i=0; i<3; i++){
                for(int j=0; j<3; j++){
                    jButtons[i][j].setText("---");
                }
            }
            winner.setText("");
        }
    }

    class Game extends JFrame{
        boolean isWinner(){
            //horizontal
            if(jButtons[0][0].getText() == jButtons[0][1].getText() && jButtons[0][1].getText() == jButtons[0][2].getText() && jButtons[0][0].getText() != "---")
                return true;
            else if(jButtons[1][0].getText() == jButtons[1][1].getText() && jButtons[1][1].getText() == jButtons[1][2].getText() && jButtons[1][0].getText() != "---")
                return true;
            else if(jButtons[2][0].getText() == jButtons[2][1].getText() && jButtons[2][1].getText() == jButtons[2][2].getText() && jButtons[2][0].getText() != "---")
                return true;

                //vertical
            else if(jButtons[0][0].getText() == jButtons[1][0].getText() && jButtons[1][0].getText() == jButtons[2][0].getText() && jButtons[0][0].getText() != "---")
                return true;
            else if(jButtons[0][1].getText() == jButtons[1][1].getText() && jButtons[1][1].getText() == jButtons[2][1].getText() && jButtons[0][1].getText() != "---")
                return true;
            else if(jButtons[0][2].getText() == jButtons[1][2].getText() && jButtons[1][2].getText() == jButtons[2][2].getText() && jButtons[0][2].getText() != "---")
                return true;

                //diagonal
            else if(jButtons[0][0].getText() == jButtons[1][1].getText() && jButtons[1][1].getText() == jButtons[2][2].getText() && jButtons[0][0].getText() != "---")
                return true;
            else if(jButtons[0][2].getText() == jButtons[1][1].getText() && jButtons[1][1].getText() == jButtons[2][0].getText() && jButtons[0][2].getText() != "---")
                return true;
            else return false;
        }

        public String whoIsWinner(){
            if(turn%2 == 0) return ("0");
            else return ("X");
        }
    }
}
