package lab9.ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class FileContent extends JFrame {
    JTextField fileName;
    JButton OKButton;
    JTextArea fileContent;

    FileContent(){
        setTitle("Display the file content!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500,500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        fileName = new JTextField();
        fileName.setBounds(50,30,400,50);
        fileName.setText("Enter the file name!");
        fileName.setHorizontalAlignment(JTextField.CENTER);

        OKButton = new JButton("OK!");
        OKButton.setBounds(220,100,60,25);

        OKButton.addActionListener(new ButtonAction());

        fileContent = new JTextArea();
        fileContent.setBounds(50,200,400,250);
        fileContent.setText("The content will be displayed here!");

        add(fileName);
        add(OKButton);
        add(fileContent);
    }

    public static void main(String[] args) {
        new FileContent();
    }

    class ButtonAction implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            String fileNameValue = fileName.getText();
            String line = "";
            String text = "";

            File file = new File(fileNameValue);
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(file);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            while (true) {
                try {
                    if (!((line=bufferedReader.readLine()) != null)) break;
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

                for(int i = 0; i<line.length(); i++){
                    text+=line.charAt(i);
                }

                fileContent.setText(text);
            }
        }
    }
}
