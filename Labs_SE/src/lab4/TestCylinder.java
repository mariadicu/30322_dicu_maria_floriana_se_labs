package lab4;

public class TestCylinder {
    public static void main(String[] args) {
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.3, 5.0);

        Circle circle1 = new Circle();
        Circle circle2 = new Circle(9, "pink");

        System.out.println("Height of cylinder1: " + cylinder1.getHeight());
        System.out.println("Radius of cylinder2: " + cylinder2.getRadius());
        System.out.println("Height of cylinder2: " + cylinder2.getHeight());
        System.out.println("Radius of circle2: " + circle2.getRadius());
        System.out.println("Area of cylinder1: " + cylinder1.getArea());
        System.out.println("Area of circle1: " + circle1.getArea());
        System.out.println("Volume of cylinder2: " + cylinder2.getVolume());

    }
}
