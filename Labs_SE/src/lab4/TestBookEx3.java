package lab4;

public class TestBookEx3 {
    public static void main(String[] args) {
        Author author1 = new Author("author1","author1@yahoo.com",'m');
        Author author2 = new Author("author2","author2@yahoo.com",'m');

        BookEx3 b1 = new BookEx3("book1",author1,50,200);
        BookEx3 b2 = new BookEx3("book2",author2,100);

        //Testing methods
        System.out.println("Name of book b1: "+b1.getName());
        System.out.println("Author of book b1: "+b1.getAuthor().getName());
        System.out.println("Price of book b1: "+b1.getPrice());
        System.out.println("Quantity in stock of book b1: "+b1.getQtyInStock());
        System.out.println(b1.toString());

        b1.setPrice(10);
        b1.setQtyInStock(1);
        System.out.println(b1.toString());


        System.out.println("Name of b2: "+b2.getName());
        System.out.println("Author of book b2: "+b2.getAuthor().getName());
        System.out.println("Price of book b2: "+b2.getPrice());
        System.out.println("Quantity in stock of book b2: "+b2.getQtyInStock());
        System.out.println(b2.toString());

        b2.setPrice(50);
        b2.setQtyInStock(10);
        System.out.println(b2.toString());
    }
}
