package lab4;


public class BookEx3 {
    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    public BookEx3(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public BookEx3(String name, Author author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return (this.name + " by " + author.getName() + " ( " + author.getGender() + " ) at " + author.getEmail());
    }

}
