package lab4;

public class TestCircle {
    public static void main(String[] args) {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.4, "purple");
        System.out.println("The radius of the circle1 is: " + circle1.getRadius());
        System.out.println("The area of the circle2 is: " + circle2.getArea());
    }
}
