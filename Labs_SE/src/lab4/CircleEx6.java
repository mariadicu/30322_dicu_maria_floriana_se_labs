package lab4;

public class CircleEx6 extends Shape {
    private double radius;

    CircleEx6() {
        this.radius = 1.0;
    }

    CircleEx6(double radius) {
        this.radius = radius;
    }

    CircleEx6(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * radius * radius;
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return ("A Circle with radius = " + radius + ", which is a subclass of " + super.toString());
    }

}
