package lab4;

public class TestShape {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3, 5);
        Rectangle rectangle3 = new Rectangle(2, 7, "pink", true);

        CircleEx6 circle1 = new CircleEx6();
        CircleEx6 circle2 = new CircleEx6(6);
        CircleEx6 circle3 = new CircleEx6(4, "blue", false);

        Square square1 = new Square(10);
        Square square2 = new Square(20, "red", true);
    }
}