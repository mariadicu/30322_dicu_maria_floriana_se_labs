package lab4;


public class Cylinder extends Circle{
    private double height;

    public Cylinder() {
        this.height = 1.0;
    }

    public Cylinder(double radius) {
        super.getRadius();
    }

    public Cylinder(double radius, double height) {
        super.setRadius(radius);
        this.height = height;
    }

    public double getHeight(){
        return this.height;
    }

    public double getArea() {
        return 2 * Math.PI * getRadius() * this.height + 2 * Math.PI * getRadius() * getRadius();
    }

    public double getVolume() {
        return this.getArea() * this.height;
    }

}
