package lab4;

public class Square extends Rectangle{
    Square() {}

    Square(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    Square(double side, String color, boolean filled) {
        super.setLength(side);
        super.setWidth(side);
        super.setColor(color);
        super.setFilled(filled);
    }

    public double getSide() {
        return getLength();
    }

    public void setSide(double side) {
        super.setLength(side);
    }

    @Override
    public void setWidth(double side) {
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return ("A Square with side = " + getSide() + ", which is a subclass of " + super.toString());
    }
}
