package lab4;

import lab4.Author;
import java.util.Arrays;

public class TestBookEx4 {
    public static void main(String[] args) {
        Author[] author1 = new Author[2];
        Author[] author2 = new Author[1];

        author1[0] = new Author("Bob", "bob@yahoo.com", 'm');
        author1[1] = new Author("Jane", "jane@yahoo.com", 'f');
        author2[0] = new Author("John", "john@yahoo.com", 'm');

        BookEx4[] book = new BookEx4[2];

        book[0] = new BookEx4("Title1", author1, 10, 1000);
        book[1] = new BookEx4("Title2", author2, 7.5);

        System.out.println("Book1 title: " + book[0].getName());
        System.out.println("Book2 author: " + Arrays.toString(book[1].getAuthor()));
        System.out.println("Book2 price: " + book[1].getPrice());
        System.out.println("Book1 qtyInStock: " + book[0].getQtyInStock());

        book[0].setQtyInStock(2000);
        System.out.println("Book1 qtyInStock: " + book[0].getQtyInStock());

        book[1].setPrice(8);
        System.out.println("Book2 price: " + book[1].getPrice());
        System.out.println(book[0].toString());

        book[0].printAuthors(author1);
        book[1].printAuthors(author2);
    }
}
