package lab4;

public class TestAuthor {
    public static void main(String[] args){
        Author author1 = new Author("Bob", "bob@yahoo.com", 'm');
        System.out.println("Name: " + author1.getName());
        System.out.println("Email: " + author1.getEmail());
        System.out.println("Gender: " + author1.getGender());
        author1.setEmail("bobby@yahoo.com");
        System.out.println("Email: " + author1.getEmail());
        System.out.println(author1.toString());
    }
}
