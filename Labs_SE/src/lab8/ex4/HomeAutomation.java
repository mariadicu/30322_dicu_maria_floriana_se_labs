package lab8.ex4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class HomeAutomation {

    public static void main(String[] args) throws IOException {
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("C:\\Users\\Maria\\Desktop\\Bitbucket_Dicu_Maria_30322\\30322_dicu_maria_floriana_se_labs\\Labs_SE\\src\\lab8\\ex4\\Test"));

            //test using an annonimous inner class
            Home h = new Home() {
                protected void setValueInEnvironment(Event event) {
                    System.out.println("New event in environment " + event);
                    try {
                        out.write("New event in environment " + event+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                protected void controllStep() {
                    System.out.println("Control step executed");
                    try {
                        out.write("Control step executed"+"\n");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            h.simulate();
            out.close();
        } catch(IOException e)
        {
            e.printStackTrace();
        }
    }
}

class HeatingUnit {


    @Override
    public String toString() {
        return "HEATING";
    }
}

class GsmUnit {


    @Override
    public String toString() {
        return "GSM";
    }
}

class CoolingUnit {
    @Override
    public String toString() {
        return "COOLER";
    }
}

class ControlUnit {
    private static ControlUnit control;
    private ControlUnit()
    {
    }
    public static ControlUnit getControl()
    {
        if(control==null)
            control=new ControlUnit();
        return control;
    }
}

class Alarm {


    @Override
    public String toString() {
        return "ALARM";
    }
}

abstract class Home {
    private Random r = new Random();
    private final int SIMULATION_STEPS = 10;

    protected abstract void setValueInEnvironment(Event event) throws IOException;
    protected abstract void controllStep() throws IOException;

    private Event getHomeEvent() {
        //randomly generate a new event;
        int k = r.nextInt(100);
        if (k < 30)
            return new NoEvent();
        else if (k < 60) {
            return new FireEvent(r.nextBoolean());

        }
        else
            return new TemperatureEvent(r.nextInt(50));
    }

    public void simulate() throws IOException {
        int k = 0;
        while(k <SIMULATION_STEPS){
            Event event = this.getHomeEvent();
            setValueInEnvironment(event);
            controllStep();

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }

}

class FireEvent extends Event {

    private boolean smoke;

    FireEvent(boolean smoke) {
        super(EventType.FIRE);
        this.smoke = smoke;



    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {

        if(isSmoke()==true) {
            return "FireEvent" + "smoke=" + smoke +" "+new Alarm().toString()+ " " +new GsmUnit().toString();
        }
        else
            return "FireEvent{" + "smoke=" + smoke +'}';
    }

}

class TemperatureEvent extends Event {

    private int vlaue;

    TemperatureEvent(int vlaue) {
        super(EventType.FIRE.TEMPERATURE);
        this.vlaue = vlaue;
    }

    int getVlaue() {
        return vlaue;
    }

    @Override
    public String toString() {
        if(getVlaue()>23)
            return "TemperatureEvent " + "value=" + vlaue +" "+ new CoolingUnit().toString();
        else
            return "TemperatureEvent " + "value=" + vlaue +" "+new HeatingUnit().toString();
    }

}

class NoEvent extends Event{

    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }

}

enum EventType {
    TEMPERATURE, FIRE, NONE;
}
