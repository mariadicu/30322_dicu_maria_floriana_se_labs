package lab10.ex3;

public class Ex3 extends Thread{

    public static void main(String[] args) {
        Counter1 c1 = new Counter1("counter1");
        Counter2 c2 = new Counter2("counter2");

        c1.run();
        c2.run();
    }
}

class Counter1 extends Thread {
    public Counter1(String string) {
        setName(string);
    }

    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(getName() + " i = " + i);
        }
        System.out.println(getName() + " ended.");
    }
}

class Counter2 extends Thread {
    public Counter2(String string) {
        setName(string);
    }

    public void run() {
        for (int i = 100; i <= 200; i++) {
            System.out.println(getName() + " i = " + i);
        }
        System.out.println(getName() + " ended.");
    }
}
