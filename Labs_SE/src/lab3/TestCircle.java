package lab3;

public class TestCircle {
    public static void main(String[] args) {
        var circle = new Circle(4.0, "blue");
        System.out.println("Color:" + circle.getColor());
        System.out.println("Radius:" + circle.getRadius());
        System.out.println("Area:" + circle.getArea());
    }
}
