package lab3;

public class Robot {
    int x;

    Robot() {
        this.x = 1;
        System.out.println("Default constructor");
    }

    public void change(int k ) {
        if(k >= 1)
            x += k;
    }

    //@Override
    public String toString(int x){
        return "x=" + x;
    }
}
