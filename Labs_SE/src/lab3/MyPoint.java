package lab3;

public class MyPoint {
    private double x;
    private double y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(double x1, double y1) {
        this.x = x1;
        this.y = y1;
    }

    public double getX() {
        return x;
    }

    public void setX(double x1) {
        this.x = x1;
    }

    public double getY() {
        return y;
    }

    public void setY(double y1) {
        this.y = y1;
    }

    public void setXY(double x1, double y1) {
        this.x = x1;
        this.y = y1;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double distance(double toX, double toY) {
        return Math.sqrt((toX - this.getX()) * (toY - this.getY()) + (toX - this.getX()) * (toX - this.getX()));
    }

    public double distance(MyPoint to) {
        return this.distance(to.getX(), to.getY());
    }

}
