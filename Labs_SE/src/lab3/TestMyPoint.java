package lab3;

public class TestMyPoint {
    public static void main(String[] args) {
        var firstPoint = new MyPoint();
        var secondPoint = new MyPoint(12.2, 3.7);
        System.out.println("Distance from " + firstPoint + " to " + secondPoint + " is " + firstPoint.distance(secondPoint));
        System.out.println("Distance from " + firstPoint + " to (66.6, 77.7) is " + firstPoint.distance(66.6, 77.7));
    }
}
