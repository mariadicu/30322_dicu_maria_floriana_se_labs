package lab11;

import java.util.Observable;

public class Product extends Observable {

    String name;
    int quantity;
    int price;

    Product(){}
    Product(String name,int price,int quantity){
        this.name=name;
        this.price=price;
        this.quantity=quantity;
        this.setChanged();
        System.out.println(1);
        this.notifyObservers();
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return "Name:"+name+" ,quantity:"+quantity+" ,price:"+price+"\n";
    }

}
