package lab11;

public class StockController {
    Product p;
    StockView view;
    public StockController(Product p, StockView view){
        p.addObserver(view);
        this.p=p;
        this.view=view;
    }

    public static void main(String[] args) {
        StockView view=new StockView();
        Product p=new Product();
        StockController controller=new StockController(p,view);
    }
}
