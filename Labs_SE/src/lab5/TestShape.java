package lab5;

public class TestShape {
    public static void main(String[] args) {
        Shape[] shape = new Shape[3];

        shape[0] = new Circle(1, "red", true);
        shape[1] = new Rectangle(3, 5, "pink", false);
        shape[2] = new Square(7, "blue", true);

        System.out.println("Area of circle is " + shape[0].getArea());
        System.out.println("Perimeter of circle is " + shape[0].getPerimeter());

        System.out.println("Area of rectangle is " + shape[1].getArea());
        System.out.println("Perimeter of rectangle is " + shape[1].getPerimeter());

        System.out.println("Area of square is " + shape[2].getArea());
        System.out.println("Perimeter of square is " + shape[2].getPerimeter());

    }
}
