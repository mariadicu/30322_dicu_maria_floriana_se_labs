package lab5;
//create RotatedImage attribute and a boolean rotate, pass the rotate in the constructor of the ProxyImage, 
//then in the display, check for the rotate, if it is true then call the display of the RotatedImage, else call the one of the RealImage
//

public class ProxyImage implements Image{
    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    public ProxyImage(Image image) {
        System.out.println("\nFrom ProxyImage constructor: ");
        image.display();
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}
