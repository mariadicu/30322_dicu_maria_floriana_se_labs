package lab5;

import javax.swing.*;

public class Square extends Rectangle {
    Square() {
        super();
    }

    Square(double side) {
        super();
        this.length = side;
        this.width = side;
    }

    Square(double side, String color, boolean filled) {
        super();
        this.length = side;
        this.width = side;
        this.color = color;
        this.filled = filled;
    }

    public double getSide() {
        return this.length;
    }

    public void setSide(double side) {
        this.length = side;
        this.width = side;
    }

    @Override
    public String toString() {
        return ("This is the Square class.");
    }
}
