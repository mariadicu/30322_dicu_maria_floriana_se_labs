package lab5.ex3_4;

public class Test {
    public static void main(String[] args){
        // Controller controller = new Controller();
        // controller.control();

        Controller firstController = Controller.getController();
        firstController.control();

        // the secondController has the same address as firstController (these 2 are the same)
        // Controller secondController = Controller.getController();
        // secondController.control();
    }
}
