package lab5.ex3_4;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    @Override
    public int readValue(){
        Random random = new Random();
        return random.nextInt(100);
    }
}
