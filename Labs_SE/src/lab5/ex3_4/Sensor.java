package lab5.ex3_4;

public abstract class Sensor {
    private String location;

    public abstract int readValue();
    public String getLocation(){
        return location;
    }
}
