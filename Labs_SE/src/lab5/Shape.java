package lab5;

public abstract class Shape {
    protected String color;
    protected boolean filled;

    Shape() {
        this.color = "black";
        this.filled = true;
    }

    Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        return ("This is the Shape class.");
    }

    abstract double getArea();
    abstract double getPerimeter();

}
