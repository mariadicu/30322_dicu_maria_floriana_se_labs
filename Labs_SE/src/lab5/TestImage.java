package lab5;

import java.util.ArrayList;

public class TestImage {
    public static void main(String[] args) {

        ArrayList<Image> images = new ArrayList<>();
        images.add(new RealImage("image1"));
        images.add(new ProxyImage("image2"));
        images.add(new RotatedImage("image3"));

        images.get(0).display();
        images.get(1).display();
        images.get(2).display();

        images.add(new ProxyImage(images.get(0)));
        images.add(new ProxyImage(images.get(2)));


    }
}
