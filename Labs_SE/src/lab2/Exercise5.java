package lab2;

// Write a program which generate a vector of 10 int elements, sort them using bubble sort method and then display the result.

import java.util.Random;

public class Exercise5 {
    public static void main(String[] args){
        int[] a = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int i, j;
        Random rand = new Random();
        for(i = 0; i < 10; i++) {
            a[i] = rand.nextInt(20);
            System.out.println(a[i]+ " ");
        }
        for( i = 0; i < 9; i++)
            for(j = 0; j < 10 - i - 1; j++)
                if(a[j] >a[j+1]) {
                    int aux = a[j];
                    a[j] = a[j+1];
                    a[j+1] = aux;
                }
        System.out.println("sorted array");
        for(i = 0; i<=9; i++) {
            System.out.println(a[i]+ " ");
        }
    }
}
