package lab2;

// Write a “Guess the number” game in Java. Program will generate a random number and will ask user to guess it.
// If user guess the number program will stop. If user do not guess it program will display: 'Wrong answer, your number it too high'
// or 'Wrong answer, your number is too low'. Program will allow user maximum 3 retries after which will stop with message 'You lost'.

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void main(String[] args) {
        Random rand = new Random();
        int tries;
        try (Scanner in = new Scanner(System.in)) {
            int random_nr = rand.nextInt(20), nr;
            tries = 3;

            while (tries != 0) {
                System.out.println("\nEnter your guess: ");
                nr = in.nextInt();
                if (nr == random_nr) {
                    System.out.println("Congrats! You won!");
                    break;
                } else if (nr > random_nr) {
                    System.out.println("\nWrong answer, your number is too high");
                    tries--;
                } else {
                    System.out.println("\nWrong answer, your number is too low");
                    tries--;
                }
            }
        }
        if(tries == 0)
            System.out.println("You lost!");
    }
}