package lab2;

// Giving a vector of N elements, display the maximum element in the vector.

import java.util.Collections;
import java.util.Vector;

public class Exercise4 {
    public static void main(String args[]) {
        Vector vec = new Vector();
        vec.add(43);
        vec.add(5);
        vec.add(24);
        vec.add(-2);
        vec.add(35);
        System.out.println("The Vector elements are: " + vec);
        System.out.println("The maximum element of the Vector is: " + Collections.max(vec));
    }
}
