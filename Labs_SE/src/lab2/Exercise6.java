package lab2;

// Being given an int number N, compute N! using 2 methods:
// 1) a non recursive method
// 2) a recursive method

public class Exercise6 {
    public static int factorial(int N)
    {
        if (N <= 1)
            return 1;
        else
            return N * factorial(N - 1);
    }
    public static void main(String[] args){
        int N = 6, exp = 1;
        for(int i = 1; i<= N; i++)
            exp = exp*i;
        System.out.println(exp);
        exp = factorial(N);
        System.out.println(exp);
    }
}
