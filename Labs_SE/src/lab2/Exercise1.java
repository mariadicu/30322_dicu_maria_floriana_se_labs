package lab2;

// Write a program which reads 2 numbers from keyboard and display the maximum between them.

import java.util.Scanner;

public class Exercise1 {
    public static void main(String[] args) {
        System.out.println("Enter the numbers: ");
        Scanner read = new Scanner(System.in);
        int a, b;
        a = read.nextInt();
        b = read.nextInt();
        if(a > b)
            System.out.println(a);
        else System.out.println(b);
    }
}
