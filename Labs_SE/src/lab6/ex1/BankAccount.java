package lab6.ex1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        this.balance = balance - amount;
    }
    public void deposit(double amount){
        this.balance = balance + amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}
