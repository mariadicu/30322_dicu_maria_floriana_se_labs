package lab6.ex1;

public class TestBankAccount {
    public static void main(String[] args){
        BankAccount b1 = new BankAccount("Popescu Ion", 5000);
        BankAccount b2 = new BankAccount("Ionescu Maria", 7000);
        BankAccount b3 = new BankAccount("Popescu Ion", 5000);
        BankAccount b4 = new BankAccount("Popescu Ion", 8000);
        System.out.println(b1.equals(b2));
        System.out.println(b1.equals(b3));
        System.out.println(b1.equals(b4));
        System.out.println(b1.hashCode());
    }
}

