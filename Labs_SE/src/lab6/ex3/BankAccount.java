package lab6.ex3;

import java.util.Objects;

public class BankAccount implements Comparable{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        this.balance = balance-amount;
    }
    public void deposit(double amount){
        this.balance = balance+amount;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getOwner(){
        return this.owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public int compareTo(Object o){
        BankAccount b = (BankAccount) o;
        if(balance>b.balance) return 1;
        if(balance==b.balance) return 0;
        return -1;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}
