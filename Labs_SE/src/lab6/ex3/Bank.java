package lab6.ex3;

import java.util.Comparator;
import java.util.TreeSet;

public class Bank{
    TreeSet<BankAccount> bankAccounts = new TreeSet<BankAccount>(new balanceComp());

    public void addAccount(String owner, double balance){
        BankAccount b = new BankAccount(owner, balance);
        bankAccounts.add(b);
    }

    public void printAccounts(){
        TreeSet<BankAccount> sortedTree = (TreeSet<BankAccount>) bankAccounts.clone();
        for(BankAccount b:sortedTree){
            System.out.println(b.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        TreeSet<BankAccount> sortedTree = (TreeSet<BankAccount>) bankAccounts.clone();
        for(BankAccount b:sortedTree){
            if (b.getBalance() >= minBalance && b.getBalance() <= maxBalance)
                System.out.println(b.toString());
        }
    }
}

class balanceComp implements Comparator<BankAccount>{
    @Override
    public int compare(BankAccount b1, BankAccount b2){
        if (b1.getBalance() > b2.getBalance())
            return 1;
        else
            return -1;
    }
}
