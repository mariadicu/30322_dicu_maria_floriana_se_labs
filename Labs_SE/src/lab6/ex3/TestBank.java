package lab6.ex3;

import java.util.ArrayList;
import java.util.TreeSet;

public class TestBank {
    public static void main(String[] args){
        TreeSet<BankAccount> bankAccounts = new TreeSet<BankAccount>();
        Bank bank = new Bank();
        bank.addAccount("Popescu Ion", 5000);
        bank.addAccount("Ionescu Maria", 8000);
        bank.addAccount("Pop Razvan", 3000);
        bank.printAccounts();
        bank.printAccounts(2500,6000);
    }
}
