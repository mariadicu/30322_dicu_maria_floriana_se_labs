package lab6.ex4;

import java.util.HashMap;
import java.util.Iterator;

public class Dictionary {
    HashMap<Word,Definition> dictionary = new HashMap<Word,Definition>();
    public Dictionary(){
        System.out.println("Dictionary created");
    }

    public void addWord(Word word, Definition definition){
        if(dictionary.containsKey(word)){
            System.out.println("Modifying the word...");
        }
        else{
            System.out.println("Adding the word...");
        }
        dictionary.put(word,definition);
    }

    public Definition getDefinition(Word word){
        return dictionary.get(word);
    }

    public void getAllWords(){
        System.out.println("\nAll the words: ");
        Iterator it = dictionary.keySet().iterator();
        while(it.hasNext()){
            String s = it.next().toString();
            System.out.println(s);
        }
    }

    public void getAllDefinitions(){
        System.out.println("\nAll the definitions:");
        Iterator it = dictionary.values().iterator();
        while ((it.hasNext())){
            String s = it.next().toString();
            System.out.println(s);
        }
    }
}
