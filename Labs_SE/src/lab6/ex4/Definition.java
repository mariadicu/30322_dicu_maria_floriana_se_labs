package lab6.ex4;

public class Definition {
    private String definition;
    public Definition(String definition){
        this.definition = definition;
    }

    @Override
    public String toString() {
        return definition;
    }
}
