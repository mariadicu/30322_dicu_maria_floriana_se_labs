package lab6.ex4;

import java.util.DuplicateFormatFlagsException;

public class ConsoleMenu {
    public static void main(String[] args){
        Word word1 = new Word("dog");
        Definition definition1 = new Definition("A type of animal");
        Word word2 = new Word("Honda");
        Definition definition2 = new Definition("A type of car");
        Dictionary dictionary = new Dictionary();
        dictionary.addWord(word1,definition1);
        dictionary.addWord(word2,definition2);
        System.out.println("The definition of the word: " + word1 + " is: " + dictionary.getDefinition(word1));
        dictionary.getAllWords();
        dictionary.getAllDefinitions();
    }
}
