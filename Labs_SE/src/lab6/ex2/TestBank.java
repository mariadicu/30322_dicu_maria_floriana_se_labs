package lab6.ex2;

import java.util.ArrayList;
import java.util.List;

public class TestBank {
    public static void main(String[] args){
        ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();
        Bank bank = new Bank();
        bank.addAccount("Popescu Ion", 5000);
        bank.addAccount("Ionescu Maria", 8000);
        bank.addAccount("Pop Razvan", 3000);
        bank.printAccounts();
        bank.printAccounts(2500,6000);
        bank.getAllAccounts();
    }
}
