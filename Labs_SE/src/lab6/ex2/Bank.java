package lab6.ex2;

import java.util.*;

public class Bank{
    ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();

    public void addAccount(String owner, double balance) {
        BankAccount bankAccount = new BankAccount(owner, balance);
        bankAccounts.add(bankAccount);
    }

    public void printAccounts() {
        bankAccounts.sort(Comparator.comparing(BankAccount::getBalance));
        for(Object bankAccount : bankAccounts) {
            System.out.println(bankAccount.toString());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for(int i = 0; i < bankAccounts.size(); i++){
            if(bankAccounts.get(i).getBalance() >= minBalance && bankAccounts.get(i).getBalance() <= maxBalance){
                System.out.println(bankAccounts.get(i).toString());
            }
        }
    }

    public void getAllAccounts() {
        bankAccounts.sort(Comparator.comparing(BankAccount::getOwner));
        for(Object b : bankAccounts){
            System.out.println(b.toString());
        }
    }

}
