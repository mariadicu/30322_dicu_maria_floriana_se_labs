package lab6.ex2;

import java.util.Objects;

public class BankAccount{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        this.balance = balance-amount;
    }
    public void deposit(double amount){
        this.balance = balance+amount;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getOwner(){
        return this.owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        lab6.ex2.BankAccount that = (lab6.ex2.BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}

