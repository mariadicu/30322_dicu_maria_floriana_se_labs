package lab7.ex1;

public class CoffeeTest {
    public static void main(String[] args) throws CoffeeMakerException {
        CoffeeMaker mk = new CoffeeMaker();
        CoffeeDrinker d = new CoffeeDrinker();
        for(int i = 0; i < 15; i++){
            Coffee c = mk.makeCoffee();
            try {
                d.drinkCoffee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception: " + e.getMessage() + " temp = " + e.getTemp());
            } catch (ConcentrationException e) {
                System.out.println("Exception: " + e.getMessage()+" conc = " + e.getConc());
            }
            finally {
                System.out.println("Throw the coffee cup. \n");
            }
        }
    }
}

class CoffeeMaker{
    int count = 0;

    Coffee makeCoffee() throws CoffeeMakerException{
        System.out.println("Make a coffee");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);
        count++;
        if(count>10)
            throw new CoffeeMakerException("Too many coffees!");
        return coffee;
    }
}

class Coffee{
    private int temp;
    private int conc;
    Coffee(int t, int c){this.temp=t;this.conc=c;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    public String toString(){
        return "[coffee temperature="+temp+":concentration="+conc+"]";
    }
}

class CoffeeDrinker{
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Coffee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Coffee concentration to high!");
        System.out.println("Drink coffee:"+c);
    }
}

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t, String msg){
        super(msg);
        this.t=t;
    }

    int getTemp(){
        return t;
    }
}

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c, String msg){
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}

class CoffeeMakerException extends Exception{
    public CoffeeMakerException(String msg){
        super(msg);
    }
}
