package lab7.ex2;

import java.io.*;
import java.util.Scanner;

public class CountChar {
    public static void main(String[] args) throws IOException{
        char character;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a character: ");
        character = scanner.nextLine().charAt(0);

        File file = new File("C:\\Users\\Maria\\Desktop\\Bitbucket_Dicu_Maria_30322\\30322_dicu_maria_floriana_se_labs\\Labs_SE\\src\\lab7\\ex2\\CountCharSE.txt");
        FileInputStream fileInputStream = new FileInputStream(file);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String line;

        int countChar = 0;

        while ((line=bufferedReader.readLine()) != null){
            for(int i=0; i<line.length(); i++){
                if(character == line.charAt(i)){
                    countChar++;
                }
            }
        }
        System.out.println(character + " appears " + countChar + " times.");
    }
}
