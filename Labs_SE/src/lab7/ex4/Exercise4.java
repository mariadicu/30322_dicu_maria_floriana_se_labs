package lab7.ex4;

import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.*;
import java.io.*;

public class Exercise4 {
    public static void main(String[] args) throws Exception{
        CarFactory carFactory = new CarFactory();

        Car a = carFactory.createCar("BMW",10000);
        Car b = carFactory.createCar("Toyota", 5000);

        carFactory.serializeCar(a,"carA.dat");
        carFactory.serializeCar(b,"carB.dat");

        Car x = carFactory.deserializeCar("carA.dat");
        Car y = carFactory.deserializeCar("carB.dat");

        System.out.println(x);
        System.out.println(y);
    }
}

class CarFactory{
    CarFactory(){}

    public Car createCar(String model, double price){
        Car car = new Car(model, price);
        System.out.println("Car created.");
        return car;
    }

    public void serializeCar(Car car, String fileName) throws IOException{
        ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(fileName));
        o.writeObject(car);
        System.out.println(car+" serialized");
    }

    public Car deserializeCar(String fileName) throws IOException, ClassNotFoundException{
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName));
        Car car = (Car)in.readObject();
        System.out.println(car+" deserialized");
        return car;
    }

}

class Car implements Serializable {
    public String model;
    public double price;

    public Car(String model, double price){
        this.model = model;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }
}